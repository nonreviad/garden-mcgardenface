#![allow(non_camel_case_types)]

pub const PROC_ALL_PIDS: u32 = 1;
extern "C" {
    pub fn proc_listpids(
        type_: u32,
        typeinfo: u32,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_listpidspath(
        type_: u32,
        typeinfo: u32,
        path: *const ::std::os::raw::c_char,
        pathflags: u32,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_pidinfo(
        pid: ::std::os::raw::c_int,
        flavor: ::std::os::raw::c_int,
        arg: u64,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_pidfdinfo(
        pid: ::std::os::raw::c_int,
        fd: ::std::os::raw::c_int,
        flavor: ::std::os::raw::c_int,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_name(
        pid: ::std::os::raw::c_int,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: u32,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_regionfilename(
        pid: ::std::os::raw::c_int,
        address: u64,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: u32,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_kmsgbuf(
        buffer: *mut ::std::os::raw::c_void,
        buffersize: u32,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_pidpath(
        pid: ::std::os::raw::c_int,
        buffer: *mut ::std::os::raw::c_void,
        buffersize: u32,
    ) -> ::std::os::raw::c_int;
}
extern "C" {
    pub fn proc_libversion(
        major: *mut ::std::os::raw::c_int,
        minor: *mut ::std::os::raw::c_int,
    ) -> ::std::os::raw::c_int;
}

pub type __int32_t = ::std::os::raw::c_int;
pub type __darwin_pid_t = __int32_t;
pub type pid_t = __darwin_pid_t;

pub const SIGKILL: u32 = 9;

extern "C" {
    pub fn kill(arg1: pid_t, arg2: ::std::os::raw::c_int) -> ::std::os::raw::c_int;
}
