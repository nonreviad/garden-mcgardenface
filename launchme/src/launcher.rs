use std::{
    ffi::{c_char, c_int, c_void},
    ptr::{null, NonNull},
};

use objc2_core_foundation::{CFArray, CFBoolean, CFDictionary, CFRetained, CFString, CFURL};
use objc2_foundation::{ns_string, NSString};

#[derive(Debug)]
pub struct LaunchError {
    pub code: c_int,
    pub error: String,
}
extern "C" {
    fn SBSLaunchApplicationForDebugging(
        bundle: NonNull<CFString>,
        open_url: Option<NonNull<CFURL>>,
        arguments: NonNull<CFArray>,
        environment: Option<NonNull<CFDictionary>>,
        stdout: Option<NonNull<CFString>>,
        stderr: Option<NonNull<CFString>>,
        flags: c_char,
    ) -> c_int;
    fn SBSApplicationLaunchingErrorString(_: c_int) -> NonNull<CFString>;
}

pub fn launch(bundle: &str, debug: bool) -> Result<(), LaunchError> {
    let bundle_str = CFString::from_str(bundle);
    let arguments = unsafe {
        objc2_core_foundation::CFArrayCreate(
            objc2_core_foundation::kCFAllocatorDefault,
            core::ptr::null_mut(),
            0,
            &objc2_core_foundation::kCFTypeArrayCallBacks,
        )
    }
    .unwrap();
    let code = unsafe {
        SBSLaunchApplicationForDebugging(
            CFRetained::as_ptr(&bundle_str),
            None,
            CFRetained::as_ptr(&arguments),
            None,
            None,
            None,
            if debug { 6 } else { 4 },
        )
    };
    if code == 0 {
        return Ok(());
    }
    let message = unsafe { CFRetained::from_raw(SBSApplicationLaunchingErrorString(code)) };
    let error = message.to_string();
    // unsafe{launcher_sys::get_launch_error_message_for_code(msg.as_ptr() as *mut i8, msg.len() as c_long, code)};
    Err(LaunchError { code, error })
}
