use std::process::Command;

use apple_bundle::prelude::InfoPlist;
use clap::Parser;

mod launcher;

#[derive(Parser, Debug)]
#[command(name = "launchme")]
struct Args {
    package: String,
    #[arg(long, short)]
    debug: bool,
    #[arg(default_value_t = 1234)]
    debug_port: u16,
}

fn get_exec_path(package: &str) -> Option<String> {
    if let Ok(output) = Command::new("dpkg-query").args(["-L", package]).output() {
        let lines = std::str::from_utf8(&output.stdout).expect("Output not utf8");
        let execs: Vec<String> = lines
            .lines()
            .filter(|line| line.starts_with("/usr/local/bin/") || line.ends_with("Info.plist"))
            .map(|line| {
                if line.starts_with("/usr/local/bin/") {
                    line.to_owned()
                } else {
                    let plist: InfoPlist =
                        apple_bundle::plist::from_file(line).expect("Could not read info.plist");
                    plist.launch.bundle_executable.unwrap()
                }
            })
            .collect();
        if execs.len() != 1 {
            return None;
        }
        return execs.first().map(|l| l.clone());
    } else {
        return None;
    }
}

fn launch_tool(_package: &str, debug: bool, debug_port: u16, executable: &str) {
    if !debug {
        let _ = Command::new(executable).spawn();
    } else {
        let _ = Command::new("debugserver")
            .args([&format!("0.0.0.0:{}", debug_port), executable])
            .spawn();
    }
    

}

fn launch_app(package: &str, debug: bool, debug_port: u16, executable: &str) {
    if let Err(err) = launcher::launch(package, debug) {
        println!("Error: {:?}", err);
    } else {
        println!("Successfully launched {}", package);
        if debug {
            if let Ok(pid) = fork::fork() {
                if let fork::Fork::Child = pid {
                    let _ = Command::new("debugserver")
                        .arg(&format!("0.0.0.0:{}", debug_port))
                        .args(["-a", &executable])
                        .status();
                }
            } else {
                println!("Failed to launch debugserver :(");
            }
        }
    }
}

fn main_impl(args: Args) {
    if let Some(executable) = get_exec_path(&args.package) {
        if executable.starts_with("/usr/local/bin/") {
            launch_tool(&args.package, args.debug, args.debug_port, &executable);
        } else {
            launch_app(&args.package, args.debug, args.debug_port, &executable);
        }
    } else {
        println!("Can't launch package {}.", args.package)
    }
}
fn main() {
    match Args::try_parse() {
        Ok(args) => {
            main_impl(args);
        }
        Err(err) => {
            println!("Error: {}", err);
        }
    }
}
