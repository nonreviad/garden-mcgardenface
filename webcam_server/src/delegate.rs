use objc2::{define_class, msg_send, rc::Retained, DefinedClass, MainThreadMarker, MainThreadOnly};
use objc2_av_foundation::{
    AVCaptureConnection, AVCaptureOutput, AVCaptureVideoDataOutputSampleBufferDelegate,
};
use objc2_core_media::CMSampleBuffer;
use objc2_foundation::{NSObject, NSObjectProtocol};

use crate::encoder::Encoder;

#[derive(Debug)]
pub(crate) struct DelegateData {
    encoder: Encoder,
}

define_class!(
    #[unsafe(super = NSObject)]
    #[name = "MyDelegate"]
    #[thread_kind = MainThreadOnly]
    #[ivars = DelegateData]
    #[derive(Debug)]
    pub(crate) struct VideoDelegate;
    unsafe impl NSObjectProtocol for VideoDelegate {}
    unsafe impl AVCaptureVideoDataOutputSampleBufferDelegate for VideoDelegate {
        #[allow(non_snake_case)]
        #[unsafe(method(captureOutput:didOutputSampleBuffer:fromConnection:))]
        unsafe fn captureOutput_didOutputSampleBuffer_fromConnection(
            &self,
            _output: &AVCaptureOutput,
            sample_buffer: &CMSampleBuffer,
            _connection: &AVCaptureConnection,
        ) {
            let encoder = &self.ivars().encoder;
            encoder.submit_raw_data(sample_buffer);
        }
    }
);

impl VideoDelegate {
    pub(crate) fn new(
        mtm: MainThreadMarker,
        nal_sender: tokio::sync::mpsc::Sender<(i64, Vec<u8>)>,
    ) -> Retained<Self> {
        let this = mtm.alloc();
        let this = this.set_ivars(DelegateData {
            encoder: Encoder::new(nal_sender),
        });
        unsafe { msg_send![super(this), init] }
    }
}
