use std::{
    cell::Cell,
    ffi::c_void,
    mem::MaybeUninit,
    ptr::{null, null_mut, NonNull},
};

use objc2_core_foundation::{
    kCFAllocatorDefault, kCFBooleanFalse, CFArrayGetValueAtIndex, CFBoolean, CFBooleanGetValue,
    CFDictionary, CFDictionaryGetValueIfPresent, CFString, CFType,
};
use objc2_core_media::{
    kCMSampleAttachmentKey_NotSync, kCMVideoCodecType_H264, CMBlockBufferGetDataPointer,
    CMSampleBuffer, CMSampleBufferGetDataBuffer, CMSampleBufferGetFormatDescription,
    CMSampleBufferGetImageBuffer, CMSampleBufferGetSampleAttachmentsArray,
    CMSampleBufferGetSampleTimingInfo, CMSampleTimingInfo, CMTimeConvertScale,
    CMVideoFormatDescriptionGetDimensions, CMVideoFormatDescriptionGetH264ParameterSetAtIndex,
};

use objc2_video_toolbox::{
    kVTCompressionPropertyKey_AllowFrameReordering, VTCompressionSession,
    VTCompressionSessionCreate, VTCompressionSessionEncodeFrame, VTEncodeInfoFlags,
    VTSessionSetProperty,
};

#[derive(Debug)]
pub(crate) struct Encoder {
    session: Cell<*mut VTCompressionSession>,
    width: Cell<i32>,
    height: Cell<i32>,
    nal_sender: tokio::sync::mpsc::Sender<(i64, Vec<u8>)>,
}

fn is_iframe(buffer: &CMSampleBuffer) -> bool {
    let Some(attachments_array) =
        (unsafe { CMSampleBufferGetSampleAttachmentsArray(buffer, false) })
    else {
        return true;
    };
    let dict = unsafe { CFArrayGetValueAtIndex(&attachments_array, 0) }.cast::<CFDictionary>();
    let Some(dict_ref) = (unsafe { dict.as_ref() }) else {
        return true;
    };
    let mut not_sync: *const CFBoolean = null_mut();
    let key_exists = unsafe {
        CFDictionaryGetValueIfPresent(
            dict_ref,
            kCMSampleAttachmentKey_NotSync as *const CFString as _,
            std::ptr::addr_of_mut!(not_sync) as _,
        )
    };
    if let Some(not_sync) = unsafe { not_sync.as_ref() } {
        let not_sync = unsafe { CFBooleanGetValue(not_sync) };
        !key_exists || !not_sync
    } else {
        true
    }
}
const NAL_HEADER_SIZE: usize = 4;
const NAL_HEADER: [u8; NAL_HEADER_SIZE] = [0u8, 0u8, 0u8, 1u8];

fn append_nal_header(dst: &mut Vec<u8>) {
    dst.extend_from_slice(&NAL_HEADER);
}

fn append_data(dst: &mut Vec<u8>, data: *const u8, size: usize) {
    append_nal_header(dst);
    unsafe { dst.extend_from_slice(std::slice::from_raw_parts(data, size)) };
}
#[allow(non_snake_case)]
fn append_SPS_and_PPS_if_needed(dst: &mut Vec<u8>, buffer: &CMSampleBuffer) {
    if !is_iframe(buffer) {
        return;
    }
    let Some(description) = (unsafe { CMSampleBufferGetFormatDescription(buffer) }) else {
        return;
    };
    let mut number_of_parameter_sets = 0usize;
    unsafe {
        CMVideoFormatDescriptionGetH264ParameterSetAtIndex(
            &description,
            0,
            null_mut(),
            null_mut(),
            std::ptr::addr_of_mut!(number_of_parameter_sets),
            null_mut(),
        )
    };
    for param_idx in 0..number_of_parameter_sets {
        let mut data: *const u8 = null();
        let mut size = 0usize;
        unsafe {
            CMVideoFormatDescriptionGetH264ParameterSetAtIndex(
                &description,
                param_idx,
                std::ptr::addr_of_mut!(data),
                std::ptr::addr_of_mut!(size),
                null_mut(),
                null_mut(),
            )
        };
        append_data(dst, data, size);
    }
}

const AVCC_HEADER_LENGTH: usize = 4;

extern "C-unwind" fn compression_callback(
    _output_callback_ref_con: *mut c_void,
    cookie: *mut c_void,
    _status: i32,
    _info_flags: VTEncodeInfoFlags,
    buffer: *mut CMSampleBuffer,
) {
    let Some(cookie) =
        (unsafe { (cookie as *mut tokio::sync::mpsc::Sender<(i64, Vec<u8>)>).as_ref() })
    else {
        println!("Cookie is null");
        return;
    };
    let Some(buffer) = (unsafe { buffer.as_ref() }) else {
        return;
    };
    let Some(data) = (unsafe { CMSampleBufferGetDataBuffer(buffer) }) else {
        return;
    };

    let mut timing_info: MaybeUninit<CMSampleTimingInfo> = MaybeUninit::uninit();
    unsafe {
        CMSampleBufferGetSampleTimingInfo(
            buffer,
            0,
            NonNull::new_unchecked(timing_info.as_mut_ptr()),
        )
    };
    let timing_info = unsafe { timing_info.assume_init() };

    // h/t https://stackoverflow.com/a/28491746
    let mut elementary_stream: Vec<u8> = vec![];
    let mut block_buffer_length = 0usize;
    let mut buffer_data_pointer: *mut i8 = null_mut();
    append_SPS_and_PPS_if_needed(&mut elementary_stream, buffer);
    unsafe {
        CMBlockBufferGetDataPointer(
            &data,
            0,
            null_mut(),
            std::ptr::addr_of_mut!(block_buffer_length),
            std::ptr::addr_of_mut!(buffer_data_pointer),
        )
    };
    let mut data_slice =
        unsafe { std::slice::from_raw_parts(buffer_data_pointer as *mut u8, block_buffer_length) };
    loop {
        if data_slice.len() <= AVCC_HEADER_LENGTH {
            break;
        }
        let size = u32::from_be_bytes([data_slice[0], data_slice[1], data_slice[2], data_slice[3]])
            as usize;
        append_nal_header(&mut elementary_stream);
        elementary_stream
            .extend_from_slice(&data_slice[AVCC_HEADER_LENGTH..(AVCC_HEADER_LENGTH + size)]);
        data_slice = &data_slice[AVCC_HEADER_LENGTH + size..];
    }
    let retimed = unsafe {
        CMTimeConvertScale(
            timing_info.presentationTimeStamp,
            90_000,
            objc2_core_media::CMTimeRoundingMethod::RoundAwayFromZero,
        )
    };
    let _ = cookie.blocking_send((retimed.value, elementary_stream));
}

impl Encoder {
    fn init(&self, width: i32, height: i32) -> NonNull<VTCompressionSession> {
        let mut session: *mut VTCompressionSession = null_mut();
        unsafe {
            VTCompressionSessionCreate(
                kCFAllocatorDefault,
                width,
                height,
                kCMVideoCodecType_H264,
                None,
                None,
                kCFAllocatorDefault,
                Some(compression_callback),
                null_mut(),
                NonNull::new_unchecked(std::ptr::addr_of_mut!(session)),
            )
        };
        self.session.replace(session);
        unsafe {
            VTSessionSetProperty(
                (self.session.get() as *const CFType).as_ref().unwrap(),
                kVTCompressionPropertyKey_AllowFrameReordering,
                kCFBooleanFalse.clone().map(|v| v as &CFType),
            )
        };
        self.width.replace(width);
        self.height.replace(height);
        unsafe { NonNull::new_unchecked(session) }
    }
    pub(crate) fn new(nal_sender: tokio::sync::mpsc::Sender<(i64, Vec<u8>)>) -> Self {
        Self {
            session: Cell::new(null_mut()),
            width: Cell::new(0),
            height: Cell::new(0),
            nal_sender,
        }
    }
    pub(crate) fn submit_raw_data(&self, sample_buffer: &CMSampleBuffer) {
        let current_width = self.width.get();
        let current_height = self.height.get();
        let Some(format) = (unsafe { CMSampleBufferGetFormatDescription(sample_buffer) }) else {
            return;
        };
        let dims = unsafe { CMVideoFormatDescriptionGetDimensions(&format) };
        let Some(image_buffer) = (unsafe { CMSampleBufferGetImageBuffer(sample_buffer) }) else {
            return;
        };
        let session = if current_height != dims.height || current_width != dims.width {
            self.init(dims.width, dims.height)
        } else {
            match NonNull::new(self.session.get()) {
                Some(session) => session,
                None => self.init(dims.width, dims.height),
            }
        };
        let mut timing_info: MaybeUninit<CMSampleTimingInfo> = MaybeUninit::uninit();
        unsafe {
            CMSampleBufferGetSampleTimingInfo(
                sample_buffer,
                0,
                NonNull::new_unchecked(timing_info.as_mut_ptr()),
            )
        };
        let timing_info = unsafe { timing_info.assume_init() };
        unsafe {
            VTCompressionSessionEncodeFrame(
                session.as_ref(),
                &image_buffer,
                timing_info.presentationTimeStamp,
                timing_info.duration,
                None,
                Box::into_raw(Box::new(self.nal_sender.clone())) as _,
                null_mut(),
            )
        };
    }
}
