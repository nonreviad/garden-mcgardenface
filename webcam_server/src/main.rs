use anyhow::Result;
use axum::{
    extract::{
        ws::{Message, WebSocket},
        ConnectInfo, State, WebSocketUpgrade,
    },
    response::{Html, IntoResponse},
    routing::{any, get},
    Router,
};
use camera::Camera;
use std::{future::IntoFuture, net::SocketAddr, sync::Arc};
use tokio::sync::{mpsc::Receiver, Notify};
use webrtc::{
    api::{
        interceptor_registry::register_default_interceptors,
        media_engine::{MediaEngine, MIME_TYPE_H264},
        APIBuilder,
    },
    ice_transport::ice_server::RTCIceServer,
    interceptor::registry::Registry,
    media::Sample,
    peer_connection::{
        configuration::RTCConfiguration, sdp::session_description::RTCSessionDescription,
    },
    rtp_transceiver::{rtp_codec::RTCRtpCodecCapability, rtp_sender::RTCRtpSender},
    track::track_local::{track_local_static_sample::TrackLocalStaticSample, TrackLocal},
};
mod camera;
mod delegate;
mod encoder;

#[derive(Debug, Clone)]
struct AppState {
    video_track: Arc<TrackLocalStaticSample>,
}
async fn root() -> Html<&'static str> {
    Html(include_str!("./index.html"))
}

async fn ws_handler(
    State(state): State<AppState>,
    ws: WebSocketUpgrade,
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
) -> impl IntoResponse {
    ws.on_upgrade(move |socket| handle_socket(socket, addr, state))
}

async fn rtcp_loop(_notifier: Arc<Notify>, rtcp_sender: Arc<RTCRtpSender>) {
    let mut rtcp_buf = vec![0u8; 1500];
    while let Ok((_, _)) = rtcp_sender.read(&mut rtcp_buf).await {}
}

async fn handle_socket(mut socket: WebSocket, who: SocketAddr, state: AppState) {
    println!("Connecting to {who:?}");
    let mut m = MediaEngine::default();
    m.register_default_codecs().unwrap();
    let mut registry = Registry::new();
    registry = register_default_interceptors(registry, &mut m).unwrap();
    let api = APIBuilder::new()
        .with_media_engine(m)
        .with_interceptor_registry(registry)
        .build();
    // Prepare the configuration
    let config = RTCConfiguration {
        ice_servers: vec![RTCIceServer {
            urls: vec!["stun:stun.l.google.com:19302".to_owned()],
            ..Default::default()
        }],
        ..Default::default()
    };
    while let Some(msg) = socket.recv().await {
        let Ok(msg) = msg else {
            break;
        };
        let Ok(description) = msg.to_text() else {
            break;
        };
        let Ok(peer_connection) = api.new_peer_connection(config.clone()).await else {
            continue;
        };
        let peer_connection = Arc::new(peer_connection);
        let done_notifier = Arc::new(tokio::sync::Notify::new());
        let peer_done_notifier = Arc::clone(&done_notifier);
        let rtcp_sender = peer_connection
            .add_track(Arc::clone(&state.video_track) as Arc<dyn TrackLocal + Send + Sync>)
            .await
            .unwrap();
        let rtcp_done_notifier = Arc::clone(&done_notifier);
        tokio::spawn(rtcp_loop(rtcp_done_notifier, rtcp_sender));
        peer_connection.on_ice_connection_state_change(Box::new(move |ice_state| {
            match ice_state {
                webrtc::ice_transport::ice_connection_state::RTCIceConnectionState::Disconnected => {
                    peer_done_notifier.notify_waiters();
                },
               _ => {}
            }
            Box::pin(async{})
        }));
        #[allow(deprecated)]
        let b = base64::decode(description).unwrap();
        let Ok(description) = String::from_utf8(b) else {break;};
        let Ok(description) = serde_json::from_str::<RTCSessionDescription>(&description) else {break; };
        peer_connection
            .set_remote_description(description)
            .await
            .unwrap();
        let answer = peer_connection.create_answer(None).await.unwrap();
        let mut gather_complete = peer_connection.gathering_complete_promise().await;
        peer_connection.set_local_description(answer).await.unwrap();
        let _ = gather_complete.recv().await;
        if let Some(local_description) = peer_connection.local_description().await {
            let json_str = serde_json::to_string(&local_description).unwrap();
            #[allow(deprecated)]
            let b64 = base64::encode(&json_str);
            let _ = socket.send(Message::text(b64)).await;
        }
        done_notifier.notified().await;
        let _ = peer_connection.close().await;
    }
    println!("Disconnected from {who:?}");
}

async fn track_loop(track: Arc<TrackLocalStaticSample>, mut receiver: Receiver<(i64, Vec<u8>)>) {
    while let Some((pts, data)) = receiver.recv().await {
        let _ = track
            .write_sample(&Sample {
                data: data.into(),
                packet_timestamp: pts as u32,
                ..Default::default()
            })
            .await;
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let (nal_sender, nal_receiver) = tokio::sync::mpsc::channel(1024);
    let video_track = Arc::new(TrackLocalStaticSample::new(
        RTCRtpCodecCapability {
            mime_type: MIME_TYPE_H264.to_owned(),
            ..Default::default()
        },
        "video".to_owned(),
        "webrtc-rs".to_owned(),
    ));
    let app = Router::new()
        .route("/ws", any(ws_handler))
        .route("/", get(root))
        .with_state(AppState {
            video_track: Arc::clone(&video_track),
        });
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await?;
    let axum_handle = tokio::spawn(
        axum::serve(
            listener,
            app.into_make_service_with_connect_info::<SocketAddr>(),
        )
        .into_future(),
    );
    tokio::spawn(track_loop(Arc::clone(&video_track), nal_receiver));
    let camera = Camera::new(nal_sender);
    camera.run();
    let _ = axum_handle.await?;
    Ok(())
}
