#![allow(non_upper_case_globals)]
#[macro_use]
extern crate dotenv_codegen;
use power::RustBatteryChargeState;
use sqlx::{mysql::MySqlPoolOptions, MySql, Pool};
use std::{
    process::exit,
    time::Duration,
};

const THRESHOLD_BATTERY_LOW: f32 = 70.0;
const THRESHOLD_BATTERY_HIGH: f32 = 90.0;

const SLEEP_TIME_NORMAL: u64 = 30;

fn get_device() -> String {
    gethostname::gethostname().into_string().unwrap().to_ascii_lowercase()
}

async fn battery_maintenance_loop(pool: Pool<MySql>) {
    let device = get_device();
    loop {
        let battery_state = power::get_battery_state();
        let _ = sqlx::query!(
            r#"INSERT into battery (device, percentage, charge_state) VALUES (?, ?, ?)"#,
            device,
            battery_state.percentage,
            battery_state.charge_state
        )
        .execute(&pool)
        .await;
        if battery_state.percentage > THRESHOLD_BATTERY_HIGH
            && battery_state.charge_state != RustBatteryChargeState::Unplugged
        {
            power::unplug().await;
        } else if battery_state.percentage < THRESHOLD_BATTERY_LOW
            && battery_state.charge_state != RustBatteryChargeState::Charging
        {
            power::plug().await;
        }
        tokio::time::sleep(Duration::from_secs(SLEEP_TIME_NORMAL)).await;
    }
}

async fn temp_sensor_maintenance_loop(pool: Pool<MySql>) {
    let device = get_device();
    loop {
        let temps = sensors::read_temps();
        for (name, temp) in temps.iter() {
            let _ = sqlx::query!(
                "INSERT into temps (device, name, temp) values (?, ?, ?)",
                device,
                name,
                temp
            )
            .execute(&pool)
            .await;
        }
        tokio::time::sleep(Duration::from_secs(SLEEP_TIME_NORMAL)).await;
    }
}

async fn main_loop(db_url: &str) {
    let pool = MySqlPoolOptions::new()
        .max_connections(5)
        .connect(&db_url)
        .await
        .unwrap();
    let temps_thread = tokio::spawn(temp_sensor_maintenance_loop(pool.clone()));
    let battery_thread = tokio::spawn(battery_maintenance_loop(pool.clone()));
    let _ = tokio::join!(temps_thread, battery_thread);
}
#[tokio::main]
async fn main() {
    let db_url = dotenv!("DATABASE_URL");
    processes::list_processes()
    .into_iter()
    .filter(|proc| {
        proc.name == ("/usr/local/bin/maintenance") && proc.pid != (std::process::id() as i32)
    })
    .for_each(|proc| {
        if unsafe { processes::processes_sys::kill(proc.pid, processes::processes_sys::SIGKILL as i32) } != 0 {
            println!("Failed to kill existing maintenance process");
            exit(-1);
        }
    });
    main_loop(db_url).await;
}
