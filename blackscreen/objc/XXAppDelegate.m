#import "XXAppDelegate.h"

@implementation XXAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	_window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	_window.rootViewController = [[UIViewController alloc] init];
	[_window makeKeyAndVisible];
	_window.windowLevel = UIWindowLevelStatusBar;
	return YES;
}

@end