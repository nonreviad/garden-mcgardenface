#import <Foundation/Foundation.h>
#import "XXAppDelegate.h"

int objc_startup(int argc, char* argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass(XXAppDelegate.class));
	}
}