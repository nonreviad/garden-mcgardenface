use std::ffi::c_int;

extern "C" {
    pub fn objc_startup(argc: c_int, argv: *const *const u8) -> c_int;
}
fn main() {
    let args: Vec<_> = std::env::args().map(|arg| arg.as_ptr()).collect();
    unsafe { objc_startup(args.len() as i32, args.as_ptr()) };
}
