fn main() {
    let mut objc = objc_rs::ObjC::new();
    objc.source("objc/binding.m");
    objc.source("objc/XXAppDelegate.m");
    objc.include_dir("objc");
    objc.framework("UIKit");
    objc.compile("blackscreen_native");
}