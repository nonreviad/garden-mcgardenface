#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct TempSensor {
    pub name: [::std::os::raw::c_char; 64usize],
    pub val: f64,
}
impl TempSensor {
    pub fn name_str(&self) -> String {
        let data: Vec<u8> = self.name.iter().cloned().map(|x| x as u8).take_while(|x| *x != 0).collect();
        String::from_utf8(data).unwrap()
    }
}

extern "C" {
    pub fn getTemperatureSensorCount() -> ::std::os::raw::c_long;
}

extern "C" {
    pub fn readTemperatureSensorsInPlace(
        sensor_data: *mut TempSensor,
        max_sensors: ::std::os::raw::c_long,
    );
}