#include "sensors.h"

#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#include <math.h>
#include <string.h>
#define IOHIDEventFieldBase(type)   (type << 16)
#define kIOHIDEventTypeTemperature  15

typedef struct __attribute__((objc_bridge(id))) __IOHIDEventSystemClient * IOHIDEventSystemClientRef;
IOHIDEventSystemClientRef _Nonnull IOHIDEventSystemClientCreateSimpleClient(CFAllocatorRef _Nullable allocator);
Boolean IOHIDEventSystemClientSetProperty(IOHIDEventSystemClientRef _Nonnull client, CFStringRef _Nonnull key, CFTypeRef _Nonnull property);
CFTypeRef _Nullable IOHIDEventSystemClientCopyProperty(IOHIDEventSystemClientRef _Nonnull client, CFStringRef _Nonnull key);
CFTypeID IOHIDEventSystemClientGetTypeID(void);
CFArrayRef _Nullable IOHIDEventSystemClientCopyServices(IOHIDEventSystemClientRef _Nonnull client);

typedef struct __IOHIDEvent *IOHIDEventRef;
typedef struct __IOHIDServiceClient *IOHIDServiceClientRef;
#ifdef __LP64__
typedef double IOHIDFloat;
#else
typedef float IOHIDFloat;
#endif

IOHIDEventSystemClientRef _Nullable IOHIDEventSystemClientCreate(CFAllocatorRef _Nullable allocator);
int IOHIDEventSystemClientSetMatching(IOHIDEventSystemClientRef _Nonnull client, CFDictionaryRef _Nonnull match);
IOHIDEventRef _Nullable IOHIDServiceClientCopyEvent(IOHIDServiceClientRef _Nonnull, int64_t , int32_t, int64_t);
CFStringRef _Nullable IOHIDServiceClientCopyProperty(IOHIDServiceClientRef _Nonnull service, CFStringRef _Nonnull property);
IOHIDFloat IOHIDEventGetFloatValue(IOHIDEventRef _Nonnull event, int32_t field);



CFDictionaryRef matching(int page, int usage)
{
    CFNumberRef nums[2];
    CFStringRef keys[2];
    
    keys[0] = CFStringCreateWithCString(0, "PrimaryUsagePage", 0);
    keys[1] = CFStringCreateWithCString(0, "PrimaryUsage", 0);
    nums[0] = CFNumberCreate(0, kCFNumberSInt32Type, &page);
    nums[1] = CFNumberCreate(0, kCFNumberSInt32Type, &usage);
    
    CFDictionaryRef dict = CFDictionaryCreate(0, (const void**)keys, (const void**)nums, 2, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    return dict;
}

CFArrayRef tempSensors() {
    IOHIDEventSystemClientRef s = IOHIDEventSystemClientCreate(kCFAllocatorDefault);
    IOHIDEventSystemClientSetMatching(s, matching(0xff00, 5));
    return IOHIDEventSystemClientCopyServices(s);
}

long getTemperatureSensorCount() {
    return CFArrayGetCount(tempSensors());
}
void readTemperatureSensorsInPlace(struct TempSensor* sensor_data, long max_sensors) {
    CFArrayRef sensors = tempSensors();
    long final_count = CFArrayGetCount(sensors);
    if (final_count > max_sensors) {
        final_count = max_sensors;
    }
    for (long i = 0; i < final_count; ++i, ++sensor_data) {
        IOHIDServiceClientRef service_client = (IOHIDServiceClientRef)CFArrayGetValueAtIndex(sensors, i);
        IOHIDEventRef temp = IOHIDServiceClientCopyEvent(service_client, kIOHIDEventTypeTemperature, 0, 0);
        if (temp) {
            sensor_data->val = IOHIDEventGetFloatValue(temp, IOHIDEventFieldBase(kIOHIDEventTypeTemperature));
        } else {
            sensor_data->val = 0.0;
        }
        CFStringRef name = IOHIDServiceClientCopyProperty(service_client, CFSTR("Product"));
        if (name == nil) {
            strcpy(sensor_data->name, "unknown");
        } else {
            CFStringGetCString(name, sensor_data->name, sizeof(sensor_data->name), kCFStringEncodingUTF8);
        }
    }
}
