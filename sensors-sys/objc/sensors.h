#pragma once
struct TempSensor {
    char name[64];
    double val;
};

long getTemperatureSensorCount();
void readTemperatureSensorsInPlace(struct TempSensor* sensor_data, long max_sensors);