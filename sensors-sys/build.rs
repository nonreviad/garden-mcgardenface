fn main() {
    let mut objc = objc_rs::ObjC::new();
    objc.source("objc/sensors.m");
    objc.include_dir("objc");
    objc.framework("IOKit");
    objc.framework("UIKit");
    objc.compile("sensors_native");
}