#![allow(non_upper_case_globals)]
#[allow(unused)]

pub const BatteryChargeState_Unknown: BatteryChargeState = 0;
pub const BatteryChargeState_Unplugged: BatteryChargeState = 1;
pub const BatteryChargeState_Charging: BatteryChargeState = 2;
pub const BatteryChargeState_Full: BatteryChargeState = 3;

pub type BatteryChargeState = ::std::os::raw::c_uint;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct BatteryState {
    pub state: BatteryChargeState,
    pub percentage: f32,
}

extern "C" {
    pub fn getBatteryState() -> BatteryState;
}
