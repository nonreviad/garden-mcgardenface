fn main() {
    let mut objc = objc_rs::ObjC::new();
    objc.source("objc/battery.m");
    objc.include_dir("objc");
    objc.framework("UIKit");
    objc.compile("battery_native");
}