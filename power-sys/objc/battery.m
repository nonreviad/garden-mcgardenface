#include "battery.h"
#import <UIKit/UIKit.h>

struct BatteryState getBatteryState() {
	UIDevice *myDevice = [UIDevice currentDevice];    
	[myDevice setBatteryMonitoringEnabled:YES];
	double batLeft = (float)[myDevice batteryLevel] * 100;
	enum BatteryChargeState chargeState = Unknown;
	switch (myDevice.batteryState) {
		case UIDeviceBatteryStateUnknown:
			chargeState = Unknown;
			break;
		case UIDeviceBatteryStateUnplugged:
			chargeState = Unplugged;
			break;
		case UIDeviceBatteryStateCharging:
			chargeState = Charging;
			break;
		case UIDeviceBatteryStateFull:
			chargeState = Full;
			break;
	}
	[myDevice setBatteryMonitoringEnabled:NO];
	struct BatteryState state;
	state.state = chargeState;
	state.percentage = batLeft;
	return state;
}