#pragma once
enum BatteryChargeState {
    Unknown,
    Unplugged,
    Charging,
    Full
};

struct BatteryState {
    enum BatteryChargeState state;
    float percentage;
};

struct BatteryState getBatteryState();
