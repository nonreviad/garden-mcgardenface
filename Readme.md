# Garden McGardenface

# What is this?

Honestly, it's only public for the lulz. I have a small garden with a greenhouse that I want to remain alive while I'm away.
To that end, I'm using:
- a small 'off grid' system consisting of a 20W solar panel, MPPT charge controller and a smøl LiFePO4 battery
- a series of jailbroken iPhones
- an ESP8266
- a 4 channel relay
- a 12V water pump

The `ESP8266` hosts a web server with two endpoints:
- http://gardesp/on?port=<port> , which will turn port `port` of the relay on
- http://gardesp/off?port=<port> , which will turn port `port` of the relay off

The relay has ports `in1`, `in2`, `in3` and `in4`.
Port `in3` controls the water pump.
Port `in4` controls the iPhone charge port.

The iPhone has several utilities:
- `launchme` is basically a clone of the `open` app. The official version in Cydia is broken, so I fully cloned a project from GitHub. I claim no originality with this code.
- `battery` is a simple cli app that will print out charge level and charge state (i.e. charging, full or unplugged)
- `maintenance` is a background app that will toggle charging on and off. This is to potentially prolongue its own battery (the iOS battery preservation measures when permanently connected to power are unknown).
- `blackscreen` just displays a black screen. Since the iPhone has no display timeout, this should save battery when nothing is going on.
- `webcam` provides an http endpoint on port 8081, which will initiate a WebRTC connection that shows the back facing camera at high quality preset; this is somewhat brittle, as medium quality is the one optimised for streaming over WiFi.
