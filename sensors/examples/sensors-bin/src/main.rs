fn main() {
    sensors::read_temps().into_iter().for_each(|(name, temp)| {
        println!("{name} @ {temp} degrees C");
    });
}
