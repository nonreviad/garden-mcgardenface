use std::collections::HashMap;
use sensors_sys::{getTemperatureSensorCount, readTemperatureSensorsInPlace, TempSensor};
pub use sensors_sys;
pub fn read_temps() -> HashMap<String, f64> {
    let cnt = unsafe { getTemperatureSensorCount() } as usize;
    let mut sensors: Vec<TempSensor> = Vec::new();
    sensors.resize(cnt, TempSensor { name: [0; 64], val: 0.0 });
    unsafe {readTemperatureSensorsInPlace(sensors.as_mut_ptr(), sensors.len() as ::std::os::raw::c_long)};
    sensors.into_iter().map(|sensor| (sensor.name_str(), sensor.val)).collect()
}
