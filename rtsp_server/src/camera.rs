use std::sync::{atomic::AtomicBool, Arc};

use dispatch2::{Queue, QueueAttribute};
use objc2::{msg_send, rc::Retained, MainThreadMarker};
use objc2_av_foundation::{
    AVCaptureDevice, AVCaptureDeviceInput, AVCaptureSession, AVCaptureSessionPreset1920x1080,
    AVCaptureVideoDataOutput,
};
use objc2_core_foundation::{CFRetained, CFRunLoop, CFRunLoopGetCurrent, CFRunLoopRun};

use crate::delegate::VideoDelegate;

#[allow(unused)]
pub(crate) struct Camera {
    session: Retained<AVCaptureSession>,
    device: Retained<AVCaptureDevice>,
    run_loop: CFRetained<CFRunLoop>,
    input: Retained<AVCaptureDeviceInput>,
    output: Retained<AVCaptureVideoDataOutput>,
    video_delegate: Retained<VideoDelegate>,
    delegate_queue: Queue,
    done: Arc<AtomicBool>,
}

impl Camera {
    pub fn new(nal_sender: tokio::sync::mpsc::Sender<(i64, Vec<u8>)>) -> Self {
        let Some(device) = (unsafe {
            objc2_av_foundation::AVCaptureDevice::defaultDeviceWithMediaType(
                objc2_av_foundation::AVMediaTypeVideo.unwrap(),
            )
        }) else {
            panic!("Impossible to get device");
        };
        let input = match unsafe {
            objc2_av_foundation::AVCaptureDeviceInput::deviceInputWithDevice_error(&device)
        } {
            Ok(input) => input,
            Err(err) => {
                panic!("Failed to create input due to {err:?}");
            }
        };
        let output = unsafe { AVCaptureVideoDataOutput::new() };
        let run_loop = unsafe { CFRunLoopGetCurrent() }.expect("No run loop?");
        let delegate_queue = Queue::new("Video Delegate", QueueAttribute::Serial);
        let video_delegate = VideoDelegate::new(MainThreadMarker::new().unwrap(), nal_sender);
        unsafe { output.setAlwaysDiscardsLateVideoFrames(true) };
        let session = unsafe { objc2_av_foundation::AVCaptureSession::new() };
        unsafe { session.setSessionPreset(AVCaptureSessionPreset1920x1080) };
        unsafe { session.beginConfiguration() };
        unsafe { session.addInput(&input) };
        unsafe { session.addOutput(&output) };
        let _: () = unsafe {
            msg_send![&output, setSampleBufferDelegate:&*video_delegate, queue: delegate_queue.as_raw()]
        };
        unsafe { session.commitConfiguration() };
        unsafe { session.startRunning() };
        Self {
            session,
            device,
            input,
            output,
            run_loop,
            video_delegate,
            delegate_queue,
            done: Arc::new(AtomicBool::new(false)),
        }
    }
    pub fn run(&self) {
        loop {
            unsafe { CFRunLoopRun() };
            if self.done.load(std::sync::atomic::Ordering::Relaxed) {
                break;
            }
        }
    }
}
