use camera::Camera;
use rtsp::RTSPResponse;
use std::{collections::HashSet, net::SocketAddr};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, UdpSocket},
};

mod camera;
mod delegate;
mod encoder;
mod rtp;
mod rtsp;
async fn rtsp_handler(
    listener: TcpListener,
    addr_sender: tokio::sync::mpsc::Sender<String>,
    socket_port_low: u16,
) {
    while let Ok((mut socket, addr)) = listener.accept().await {
        let addr_sender = addr_sender.clone();
        tokio::spawn(async move {
            if let Err(e) =
                handle_rtsp_client(&mut socket, addr, socket_port_low, addr_sender).await
            {
                eprintln!("Error handling client {}: {:?}", addr, e);
            }
        });
    }
}
#[tokio::main]
async fn main() -> std::io::Result<()> {
    let addr = "0.0.0.0:554";
    let listener = TcpListener::bind(addr).await?;
    println!("RTSP server listening on {}", addr);
    let (first, _second) = gen_socket_pair().await;
    let socket_port_low = first.local_addr().expect("msg").port();
    let (addr_sender, addr_receiver) = tokio::sync::mpsc::channel(1024);
    let (nal_sender, nal_receiver) = tokio::sync::mpsc::channel(1024);
    tokio::spawn(rtsp_handler(listener, addr_sender, socket_port_low));
    tokio::spawn(rtp_main_loop(first, nal_receiver, addr_receiver));
    let camera = Camera::new(nal_sender);
    camera.run();
    Ok(())
}

async fn rtp_main_loop(
    rtp_socket: UdpSocket,
    mut nal_receiver: tokio::sync::mpsc::Receiver<(i64, Vec<u8>)>,
    mut new_addr_receiver: tokio::sync::mpsc::Receiver<String>,
) {
    let mut ips = HashSet::new();
    let mut cnt = 0;
    loop {
        tokio::select! {
            Some((timestamp, packet)) = nal_receiver.recv() => {
                let packets = rtp::packetize_h264(&packet, &mut cnt, timestamp as u32, 1);
                for packet in packets {
                    let packet = packet.to_bytes();
                    for ip in &ips {
                        let _ = rtp_socket.send_to(&packet, ip).await;
                    }
                }
            },
            Some(ip) = new_addr_receiver.recv() => {
                if let Some(stripped) = ip.strip_prefix("-") {
                    println!("Removing IP {stripped}");
                    ips.remove(stripped);
                } else {
                    println!("Adding IP {ip}");
                    ips.insert(ip);
                }
            }
        };
    }
}

async fn gen_socket_pair() -> (UdpSocket, UdpSocket) {
    loop {
        let first = UdpSocket::bind("0.0.0.0:0").await.expect("first");
        let second = UdpSocket::bind(format!(
            "0.0.0.0:{}",
            first.local_addr().expect("msg").port() + 1
        ))
        .await;
        if let Ok(second) = second {
            return (first, second);
        }
    }
}

async fn handle_rtsp_client(
    socket: &mut tokio::net::TcpStream,
    addr: SocketAddr,
    socket_port_low: u16,
    addr_sender: tokio::sync::mpsc::Sender<String>,
) -> anyhow::Result<()> {
    let remote = addr.to_string();
    let local = socket.local_addr()?.to_string();
    println!("New RTSP session from {local} to {remote}");
    let mut buffer = [0u8; 1024];
    let mut client_port_rtp = 0;
    loop {
        let Ok(n) = socket.read(&mut buffer).await else {
            let _ = addr_sender
                .send(format!("-{}:{client_port_rtp}", addr.ip()))
                .await;
            anyhow::bail!("Failed to read");
        };
        if n == 0 {
            println!("Client {addr:?} disconnected\n\n");
            let _ = addr_sender
                .send(format!("-{}:{client_port_rtp}", addr.ip()))
                .await;
            break;
        }

        let request = String::from_utf8_lossy(&buffer[..n]);
        println!("Received: {request:?}");
        let request = rtsp::RTSPRequest::new(&request);
        match request.method {
            rtsp::RequestType::Options => {
                let response = rtsp::RTSPResponse::new().with_cseq(&request.cseq()).build();
                println!("Sending response: {response}");
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send options");
                };
            }
            rtsp::RequestType::Describe => {
                let response = rtsp::RTSPResponse::new()
                    .with_cseq(&request.cseq())
                    .with_public("SETUP, TEARDOWN, PLAY");
                let mut sdp = "v=0\r\n".to_owned();
                sdp.push_str(&format!("o=- 4206942069 0 IN IP {:?}\r\n", addr.ip()));
                sdp.push_str("s=Garden McGardenFace\r\n");
                sdp.push_str(&format!(
                    "c=IN IP {:?}\r\n",
                    socket.local_addr().unwrap().ip()
                ));
                sdp.push_str("t=0 0\r\n");
                sdp.push_str(&format!("m=video {socket_port_low} RTP/AVP 96\r\n"));
                sdp.push_str("a=rtpmap:96 H264/90000\r\n");
                sdp.push_str("a=control:track1\r\n");
                let response = response.with_body(&sdp, "application/sdp");
                let response = response.build();
                println!("Responding with {response}");
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send response");
                };
            }
            rtsp::RequestType::Setup => {
                let cseq = request.cseq();
                let session = "4206942069".to_owned();
                let transport = request.transport();
                println!("Transport: {transport:?}");
                let mut response = rtsp::RTSPResponse::new()
                    .with_cseq(&cseq)
                    .with_session(&session);

                if let Some(transport) = transport {
                    if let Some(port) = transport.client_low() {
                        client_port_rtp = port;
                        let transport = transport
                            .with_client_port(port)
                            .with_ssrc(69)
                            .with_server_port_range(socket_port_low, socket_port_low + 1);
                        response = response.with_transport(&transport);
                    }
                }
                let response = response.build();
                println!("Sending response: {response}");
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send response");
                };
            }
            rtsp::RequestType::Teardown => {
                let _ = addr_sender
                    .send(format!("-{}:{client_port_rtp}", addr.ip()))
                    .await;
                let cseq = request.cseq();
                let response = RTSPResponse::new().with_cseq(&cseq).build();
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send response");
                };
            }
            rtsp::RequestType::Play => {
                let _ = addr_sender
                    .send(format!("{}:{client_port_rtp}", addr.ip()))
                    .await;
                let cseq = request.cseq();
                let response = RTSPResponse::new().with_cseq(&cseq).build();
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send response");
                };
            }
            rtsp::RequestType::Pause => {
                let _ = addr_sender
                    .send(format!("-{}:{client_port_rtp}", addr.ip()))
                    .await;
                let cseq = request.cseq();
                let response = RTSPResponse::new().with_cseq(&cseq).build();
                let Ok(_) = socket.write_all(response.as_bytes()).await else {
                    let _ = addr_sender
                        .send(format!("-{}:{client_port_rtp}", addr.ip()))
                        .await;
                    anyhow::bail!("Failed to send response");
                };
            }
            _ => {
                println!("Method {:?} not supported yet", request.method);
                println!("Looks like this: {:?}", request);
            }
        }
    }
    Ok(())
}
