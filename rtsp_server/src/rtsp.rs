use std::collections::HashMap;

#[derive(Debug)]
pub(crate) enum RequestType {
    Describe,
    Announce,
    GetParameter,
    Options,
    Pause,
    Play,
    Record,
    Redirect,
    Setup,
    SetParameter,
    Teardown,
}

#[derive(Debug, PartialEq, Eq, Hash)]
enum HeaderType {
    Connection,
    ContentEncoding,
    ContentLanguage,
    ContentLength,
    ContentType,
    CSeq,
    ProxyRequire,
    Require,
    RTPInfo,
    Session,
    Public,
    Transport,
    Unsupported,
}

impl HeaderType {
    fn as_string(&self) -> &'static str {
        match self {
            HeaderType::Connection => "Connection",
            HeaderType::ContentEncoding => "Content-Encoding",
            HeaderType::ContentLanguage => "Content-Language",
            HeaderType::ContentLength => "Content-Length",
            HeaderType::ContentType => "Content-Type",
            HeaderType::CSeq => "CSeq",
            HeaderType::ProxyRequire => "Proxy-Require",
            HeaderType::Public => "Public",
            HeaderType::Require => "Require",
            HeaderType::RTPInfo => "RTP-Info",
            HeaderType::Session => "Session",
            HeaderType::Transport => "Transport",
            HeaderType::Unsupported => "Unsupported",
        }
    }
}
#[derive(Debug)]
pub(crate) struct RTSPRequest {
    pub method: RequestType,
    headers: HashMap<HeaderType, String>,
}

impl RTSPRequest {
    pub(crate) fn cseq(&self) -> String {
        self.headers
            .get(&HeaderType::CSeq)
            .map(|s| s.to_owned())
            .unwrap_or_else(|| "1".to_owned())
    }
    pub(crate) fn transport(&self) -> Option<Transport> {
        self.headers
            .get(&HeaderType::Transport)
            .map(|transport_str| Transport::from(transport_str))
    }
    fn parse_request_line(line: &str) -> RequestType {
        let (method, _rest) = line.split_once(' ').expect("msg");
        match method {
            _ if method == "DESCRIBE" => RequestType::Describe,
            _ if method == "ANNOUNCE" => RequestType::Announce,
            _ if method == "GET_PARAMETER" => RequestType::GetParameter,
            _ if method == "OPTIONS" => RequestType::Options,
            _ if method == "PAUSE" => RequestType::Pause,
            _ if method == "PLAY" => RequestType::Play,
            _ if method == "RECORD" => RequestType::Record,
            _ if method == "REDIRECT" => RequestType::Redirect,
            _ if method == "SETUP" => RequestType::Setup,
            _ if method == "SET_PARAMETER" => RequestType::SetParameter,
            _ if method == "TEARDOWN" => RequestType::Teardown,
            _ => panic!("Unknown request type '{method}'"),
        }
    }
    fn parse_header(header_name: &str) -> Option<HeaderType> {
        match header_name {
            _ if header_name == "Connection" => Some(HeaderType::Connection),
            _ if header_name == "Content-Encoding" => Some(HeaderType::ContentEncoding),
            _ if header_name == "Content-Language" => Some(HeaderType::ContentLanguage),
            _ if header_name == "Content-Length" => Some(HeaderType::ContentLength),
            _ if header_name == "CSeq" => Some(HeaderType::CSeq),
            _ if header_name == "Proxy-Require" => Some(HeaderType::ProxyRequire),
            _ if header_name == "Require" => Some(HeaderType::Require),
            _ if header_name == "RTP-Info" => Some(HeaderType::RTPInfo),
            _ if header_name == "Session" => Some(HeaderType::Session),
            _ if header_name == "Transport" => Some(HeaderType::Transport),
            _ if header_name == "Unsupported" => Some(HeaderType::Unsupported),
            _ => None,
        }
    }
    pub(crate) fn new(s: &str) -> Self {
        let mut headers = HashMap::new();
        let (request_line, rest) = s.split_once("\r\n").expect("No request line");
        let method = Self::parse_request_line(request_line);
        let (header_lines, _body) = rest.split_once("\r\n\r\n").expect("");
        for line in header_lines.split("\r\n") {
            let (header_name, header_content) = line.split_once(": ").expect("msg");
            if let Some(header) = Self::parse_header(header_name) {
                headers.insert(header, header_content.to_owned());
            }
        }
        Self { method, headers }
    }
}

pub(crate) struct RTSPResponse {
    headers: HashMap<HeaderType, String>,
    body: Option<String>,
}
#[derive(Debug)]
pub(crate) enum CastType {
    Unicast,
    Multicast,
}

#[derive(Debug, Default)]
pub(crate) struct Transport {
    transport_spec: String,
    cast_type: Option<CastType>,
    client_port_low: Option<u16>,
    client_port_high: Option<u16>,
    server_port_low: Option<u16>,
    server_port_high: Option<u16>,
    ssrc: Option<u64>,
}

impl Transport {
    pub(crate) fn client_low(&self) -> Option<u16> {
        self.client_port_low
    }
    pub(crate) fn with_client_port(mut self, port: u16) -> Self {
        self.client_port_low = Some(port);
        self.client_port_high = None;
        self
    }
    pub(crate) fn with_server_port_range(mut self, low: u16, high: u16) -> Self {
        self.server_port_low = Some(low);
        self.server_port_high = Some(high);
        self
    }
    pub(crate) fn with_ssrc(mut self, ssrc: u64) -> Self {
        self.ssrc = Some(ssrc);
        self
    }
    pub(crate) fn serialise(&self) -> String {
        let mut s = self.transport_spec.to_owned();
        if let Some(ref cast_type) = self.cast_type {
            match cast_type {
                CastType::Unicast => s.push_str(";unicast"),
                CastType::Multicast => s.push_str(";multicast"),
            }
        }
        if let Some(ref client_port_low) = self.client_port_low {
            s.push_str(&format!(";client_port={client_port_low}"));
        }
        if let Some(ref client_port_high) = self.client_port_high {
            s.push_str(&format!("-{client_port_high}"));
        }
        if let Some(ref server_port_low) = self.server_port_low {
            s.push_str(&format!(";server_port={server_port_low}"));
        }
        if let Some(ref server_port_high) = self.server_port_high {
            s.push_str(&format!("-{server_port_high}"));
        }
        if let Some(ref ssrc) = self.ssrc {
            s.push_str(&format!(";ssrc={ssrc}"));
        }
        s
    }
    pub(crate) fn from(s: &str) -> Self {
        println!("Parsing transport from {s}");
        let mut cast_type = None;
        let mut client_port_low = None;
        let mut client_port_high = None;
        let mut server_port_low = None;
        let mut server_port_high = None;
        let mut ssrc = None;
        let (transport_spec, params) = s.split_once(';').expect("msg");
        for param in params.split(";") {
            match param {
                _ if param == "unicast" => cast_type = Some(CastType::Unicast),
                _ if param == "multicast" => cast_type = Some(CastType::Multicast),
                _ if param.starts_with("client_port") => {
                    let (_, client_ports) = param.split_once('=').expect("msg");
                    let (low, high) = client_ports.split_once('-').expect("msg");
                    client_port_low = low.parse::<u16>().ok();
                    client_port_high = high.parse::<u16>().ok();
                }
                _ if param.starts_with("server_port") => {
                    let (_, server_ports) = param.split_once('=').expect("msg");
                    let (low, high) = server_ports.split_once('-').expect("msg");
                    server_port_low = low.parse::<u16>().ok();
                    server_port_high = high.parse::<u16>().ok();
                }
                _ if param.starts_with("ssrc") => {
                    let (_, ssrc_str) = param.split_once("=").expect("msg");
                    ssrc = ssrc_str.parse::<u64>().ok();
                }
                _ => {
                    println!("Unimplemented Transport parameter {param}");
                }
            }
        }
        Self {
            transport_spec: transport_spec.to_owned(),
            cast_type,
            client_port_low,
            client_port_high,
            server_port_low,
            server_port_high,
            ssrc,
        }
    }
}

impl RTSPResponse {
    pub(crate) fn new() -> Self {
        Self {
            headers: HashMap::new(),
            body: None,
        }
    }
    pub(crate) fn with_cseq(mut self, cseq: &str) -> Self {
        self.headers.insert(HeaderType::CSeq, cseq.to_owned());
        self
    }
    pub(crate) fn with_public(mut self, val: &str) -> Self {
        self.headers.insert(HeaderType::Public, val.to_owned());
        self
    }
    pub(crate) fn with_transport(mut self, transport: &Transport) -> Self {
        self.headers
            .insert(HeaderType::Transport, transport.serialise());
        self
    }
    pub(crate) fn with_session(mut self, val: &str) -> Self {
        self.headers.insert(HeaderType::Session, val.to_owned());
        self
    }
    pub(crate) fn with_body(mut self, body: &str, content_type: &str) -> Self {
        self.headers
            .insert(HeaderType::ContentLength, format!("{}", body.len()));
        self.headers
            .insert(HeaderType::ContentType, content_type.to_owned());
        self.body = Some(body.to_owned());
        self
    }
    pub(crate) fn build(self) -> String {
        let mut result = "RTSP/1.0 200 OK\r\n".to_owned();
        for (header, value) in self.headers {
            result.push_str(header.as_string());
            result.push_str(": ");
            result.push_str(&value);
            result.push_str("\r\n");
        }
        result.push_str("\r\n");
        if let Some(body) = self.body {
            result.push_str(&body);
        }
        result
    }
}
