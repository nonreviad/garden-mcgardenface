const RTP_HEADER_SIZE: usize = 12;
const RTP_PAYLOAD_SIZE: usize = 1200; // Adjust based on MTU

#[derive(Debug)]
pub(crate) struct RtpPacket {
    pub header: [u8; RTP_HEADER_SIZE],
    pub payload: Vec<u8>,
}

impl RtpPacket {
    fn new(seq_number: u16, timestamp: u32, ssrc: u32, marker: bool, payload: &[u8]) -> Self {
        let mut header = [0u8; RTP_HEADER_SIZE];

        // RTP version (2), Padding (0), Extension (0), CC (0)
        header[0] = 0x80;
        // Marker bit and Payload Type (H264 = 96)
        header[1] = if marker { 0xE0 } else { 0x60 };
        // Sequence number
        header[2..4].copy_from_slice(&seq_number.to_be_bytes());
        // Timestamp
        header[4..8].copy_from_slice(&timestamp.to_be_bytes());
        // SSRC
        header[8..12].copy_from_slice(&ssrc.to_be_bytes());

        RtpPacket {
            header,
            payload: payload.to_vec(),
        }
    }

    pub(crate) fn to_bytes(&self) -> Vec<u8> {
        [self.header.to_vec(), self.payload.clone()].concat()
    }
}

/// Splits an H.264 NAL unit into RTP packets.
pub(crate) fn packetize_h264(
    nal: &[u8],
    seq_number: &mut u16,
    timestamp: u32,
    ssrc: u32,
) -> Vec<RtpPacket> {
    let nal_size = nal.len();

    if nal_size <= RTP_PAYLOAD_SIZE {
        // Single NAL Unit Packet
        vec![RtpPacket::new(*seq_number, timestamp, ssrc, true, nal)]
    } else {
        // Fragmentation Unit (FU-A)
        let nal_header = nal[0];
        let fu_indicator = (nal_header & 0xE0) | 28; // FU-A indicator
        let fu_header_start = (nal_header & 0x1F) | 0x80; // Start bit set
        let fu_header_middle = nal_header & 0x1F; // Middle
        let fu_header_end = (nal_header & 0x1F) | 0x40; // End bit set

        let mut packets = Vec::new();
        let mut offset = 1;

        while offset < nal_size {
            let remaining = nal_size - offset;
            let chunk_size = remaining.min(RTP_PAYLOAD_SIZE - 2);

            let fu_header = if offset == 1 {
                fu_header_start
            } else if remaining == chunk_size {
                fu_header_end
            } else {
                fu_header_middle
            };

            let mut payload = vec![fu_indicator, fu_header];
            payload.extend_from_slice(&nal[offset..offset + chunk_size]);

            packets.push(RtpPacket::new(
                *seq_number,
                timestamp,
                ssrc,
                remaining == chunk_size,
                &payload,
            ));
            offset += chunk_size;
            *seq_number = seq_number.wrapping_add(1);
        }

        packets
    }
}
