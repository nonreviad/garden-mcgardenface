{
  description = "Development environment using Zalmoxis flake";

  inputs = {
    zalmoxis = {
      url = "gitlab:nonreviad/zalmoxis";
    };

    nixpkgs.follows = "zalmoxis/nixpkgs";
    flake-utils.follows = "zalmoxis/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, zalmoxis }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in {
        devShells.default = zalmoxis.packages.${system}.default;
      });
}
