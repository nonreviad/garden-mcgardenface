#include <ESP8266WiFi.h>
#include <map>

struct App {
    std::map<String, uint8_t> port_mappings;
    void run();
private:
    void setupPins();
    void onIp();
    void loop();
};