#include "config.h"
#include <FS.h>
#include <ArduinoJson.h>

Config Config::INSTANCE;
boolean Config::LOADED = false;

static constexpr char* CONFIG_FILE = "/config.json";

Config& Config::instance() {
    if (!LOADED) {
        auto file = SPIFFS.open(CONFIG_FILE, "r");
        StaticJsonDocument<1024> document;
        deserializeJson(document, file);
        file.close();
        if (document.containsKey("ssid")) {
            String s = document["ssid"];
            INSTANCE.ssid = s;
        }
        if (document.containsKey("password")) {
            String s = document["password"];
            INSTANCE.password = s;
        }
        if (document.containsKey("hostname")) {
            String s = document["hostname"];
            INSTANCE.hostname = s;
        }
        Config::LOADED = true;
    }
    return INSTANCE;
}
