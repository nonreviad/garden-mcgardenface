#include "app.h"
#include <Arduino.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>
#include <FS.h>
#include "config.h"
#include <ESPAsyncWebServer.h>

void App::setupPins() {
    port_mappings.emplace("in1", D7);
    port_mappings.emplace("in2", D6);
    port_mappings.emplace("in3", D5);
    port_mappings.emplace("in4", D4);
    for (const auto& [_k, v] : port_mappings) {
        pinMode(v, OUTPUT);
        digitalWrite(v, HIGH);
    }
}

void App::run() {
    setupPins();
    SPIFFS.begin();
    Config& config = Config::instance();
    Serial.printf("Connecting to %s\n", config.ssid.c_str());
    WiFi.begin(config.ssid, config.password);
    WiFi.onStationModeGotIP([this](const WiFiEventStationModeGotIP&) {onIp();});
    loop();
}

void App::onIp() {
    Serial.print("Got ip ");
    Serial.println(WiFi.localIP().toString());
    MDNS.begin(Config::instance().ssid, WiFi.localIP());
    MDNS.addService("http", "tcp", 80);
}

void htmlResponse(AsyncWebServerRequest* request, String response, int code = 200) {
    request->send(code, "text/html", response);
}

void OkieDokie(AsyncWebServerRequest* request) {
    htmlResponse(request, "<html><body><p>OK</p><body></html>");
}

void App::loop() {
    AsyncWebServer server(80);
    Serial.println("Server main loop");
    server.on("/", HTTP_GET, [this](AsyncWebServerRequest* request) {
        OkieDokie(request);
    });
    server.on("/on", HTTP_GET, [this](AsyncWebServerRequest* request) {
        AsyncWebParameter* port = request->getParam("port");
        if (!port) {
            htmlResponse(request, "<html><body><p>Please specify port</p></body></html>", 400);
        }
        const auto f = port_mappings.find(port->value());
        if (f == port_mappings.end()) {
            htmlResponse(request, "<html><body><p>Unknow port; valid ports are in1, in2, in3 and in4.</p></body></html>", 400);
        }
        digitalWrite(f->second, LOW);
        OkieDokie(request);
    });
    server.on("/off", HTTP_GET, [this](AsyncWebServerRequest* request) {
        AsyncWebParameter* port = request->getParam("port");
        if (!port) {
            htmlResponse(request, "<html><body><p>Please specify port</p></body></html>", 400);
        }
        const auto f = port_mappings.find(port->value());
        if (f == port_mappings.end()) {
            htmlResponse(request, "<html><body><p>Unknow port; valid ports are in1, in2, in3 and in4.</p></body></html>", 400);
        }
        digitalWrite(f->second, HIGH);
        OkieDokie(request);
    });

    server.begin();

    while(true) {
        MDNS.update();
        delay(1000);
    }
}