#include <Arduino.h>

struct Config {
    String ssid;
    String password;
    String hostname;

    static Config& instance();
private:
    static Config INSTANCE;
    static boolean LOADED;
    
};