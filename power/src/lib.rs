#![allow(non_upper_case_globals)]
use power_sys::{getBatteryState, BatteryChargeState};
use serde::{Deserialize, Serialize};
pub use power_sys;
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, sqlx::Type)]
pub enum RustBatteryChargeState {
    Unknown,
    Unplugged,
    Charging,
    Full,
}

impl RustBatteryChargeState {
    fn from_c(state: BatteryChargeState) -> RustBatteryChargeState {
        match state {
            power_sys::BatteryChargeState_Unplugged => RustBatteryChargeState::Unplugged,
            power_sys::BatteryChargeState_Charging => RustBatteryChargeState::Charging,
            power_sys::BatteryChargeState_Full => RustBatteryChargeState::Full,
            _ => RustBatteryChargeState::Unknown,
        }
    }
}
#[derive(Debug, Serialize, Deserialize, sqlx::Type)]
pub struct RustBatteryState {
    pub charge_state: RustBatteryChargeState,
    pub percentage: f32,
}

pub fn get_battery_state() -> RustBatteryState {
    let state = unsafe { getBatteryState() };
    RustBatteryState {
        charge_state: RustBatteryChargeState::from_c(state.state),
        percentage: state.percentage,
    }
}
pub async fn plug() {
    let _ = reqwest::get("http://gardesp/on?port=in4").await;
}

pub async fn unplug() {
    let _ = reqwest::get("http://gardesp/off?port=in4").await;
}
