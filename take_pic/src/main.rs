#![allow(non_snake_case)]
#![allow(deprecated)]

use std::{ffi::{c_void, CStr}, fs::File, io::Write, ptr::NonNull};

use clap::Parser;
use objc2::{
    define_class, msg_send,
    rc::Retained,
    runtime::{AnyObject, ProtocolObject},
    DefinedClass, MainThreadMarker, MainThreadOnly, Message,
};
use objc2_av_foundation::{
    AVCapturePhoto, AVCapturePhotoCaptureDelegate, AVCapturePhotoOutput, AVCaptureResolvedPhotoSettings, AVCaptureTorchMode, AVVideoCodecKey, AVVideoCodecTypeJPEG
};
use objc2_core_foundation::{CFRetained, CFRunLoop, CFRunLoopRun, CFRunLoopStop};
use objc2_foundation::{NSDictionary, NSError, NSObject, NSObjectProtocol, NSString};

#[derive(Debug)]
struct DelegateIVars {
    output_path: String,
    run_loop: CFRetained<CFRunLoop>
}

define_class!(
    #[unsafe(super = NSObject)]
    #[name = "MyDelegate"]
    #[ivars = DelegateIVars]
    #[thread_kind = MainThreadOnly]
    #[derive(Debug)]
    struct PhotoDelegate;
    unsafe impl NSObjectProtocol for PhotoDelegate {}

    unsafe impl AVCapturePhotoCaptureDelegate for PhotoDelegate {
        #[unsafe(method(captureOutput:willBeginCaptureForResolvedSettings:))]
        unsafe fn captureOutput_willBeginCaptureForResolvedSettings(
            &self,
            _output: &AVCapturePhotoOutput,
            _resolved_settings: &AVCaptureResolvedPhotoSettings,
        ) {
        }
        #[unsafe(method(captureOutput:didFinishProcessingPhoto:error:))]
        fn captureOutput_didFinishProcessingPhoto_error(
            &self,
            _output: &AVCapturePhotoOutput,
            photo: &AVCapturePhoto,
            error: Option<&NSError>,
        ) {
            let output_path = &self.ivars().output_path;
            let run_loop = &self.ivars().run_loop;
            if let Some(error) = error {
                println!("Oh no! {error:?}");
                let reason = unsafe {CStr::from_ptr(error.localizedDescription().UTF8String()).to_str().unwrap()};
                println!("Reason: {reason}");
            } else if let Some(data) = unsafe {photo.fileDataRepresentation()} {
                let length = data.length();
                let mut buffer = vec![0; length];
                let buffer_raw = NonNull::new(buffer.as_mut_ptr() as *mut c_void).unwrap();
                unsafe { data.getBytes_length(buffer_raw, length)};
                let mut file = File::create(output_path).unwrap();
                let _ = file.write_all(&buffer);
            } else {
                println!("Unable to get file data representation");
            }
            unsafe{ CFRunLoopStop(run_loop.as_ref())};
        }
    }
);

impl PhotoDelegate {
    fn new(mtm: MainThreadMarker, output_path: String, run_loop: CFRetained<CFRunLoop>) -> Retained<Self> {
        let this = mtm.alloc();
        let this = this.set_ivars(DelegateIVars{output_path, run_loop});
        unsafe { msg_send![super(this), init] }
    }
}
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    output: String
}
fn main() {
    let args = Args::parse();
    let session = unsafe { objc2_av_foundation::AVCaptureSession::new() };
    
    let Some(device) = (unsafe {
        objc2_av_foundation::AVCaptureDevice::defaultDeviceWithMediaType(
            objc2_av_foundation::AVMediaTypeVideo.unwrap(),
        )
    }) else {
        panic!("Impossible to get device");
    };
    let input = match unsafe {
        objc2_av_foundation::AVCaptureDeviceInput::deviceInputWithDevice_error(&device)
    } {
        Ok(input) => input,
        Err(err) => {
            panic!("Failed to create input due to {err:?}");
        }
    };
    let output = unsafe { AVCapturePhotoOutput::new() };

    
    unsafe { session.beginConfiguration() };
    let _  = unsafe { device.lockForConfiguration()} ;
    let _ = unsafe { device.setTorchMode(AVCaptureTorchMode::Auto); };
    unsafe {device.unlockForConfiguration() };
    unsafe { session.addInput(&input) };
    unsafe { session.setSessionPreset(objc2_av_foundation::AVCaptureSessionPresetPhoto) };
    unsafe { session.addOutput(&output) };
    unsafe { session.commitConfiguration() };
    unsafe { session.startRunning() };

    let settings_dict: Retained<NSDictionary<NSString, AnyObject>> = unsafe {
        NSDictionary::from_retained_objects(
            &[AVVideoCodecKey.unwrap()],
            &[AVVideoCodecTypeJPEG.unwrap().retain().into()],
        )
    };

    let settings = unsafe {
        objc2_av_foundation::AVCapturePhotoSettings::photoSettingsWithFormat(Some(&*settings_dict))
    };

    let rl = unsafe { objc2_core_foundation::CFRunLoopGetCurrent() }.unwrap();
    let delegate = PhotoDelegate::new(MainThreadMarker::new().unwrap(), args.output, rl);
    let proto = ProtocolObject::from_ref(&*delegate);
    unsafe { output.capturePhotoWithSettings_delegate(&settings, proto) };
    unsafe { CFRunLoopRun() };
    unsafe { session.stopRunning() };
}
