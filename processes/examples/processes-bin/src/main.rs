fn main() {
    processes::list_processes().iter().for_each(|process| {
        println!("{:?}", process);
    });
}
