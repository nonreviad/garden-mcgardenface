use std::{ptr::null_mut, ffi::{c_void, c_int}};

#[derive(Debug)]
pub struct Process {
    pub pid: i32,
    pub name: String
}
pub use processes_sys;
pub fn list_processes() -> Vec<Process> {
    let num_pids = unsafe{ processes_sys::proc_listpids(processes_sys::PROC_ALL_PIDS, 0, null_mut(), 0) } as usize / 4;
    let mut pids: Vec<i32> = Vec::new();
    pids.resize(num_pids, 0);
    unsafe {processes_sys::proc_listpids(processes_sys::PROC_ALL_PIDS, 0, pids.as_mut_ptr() as *mut c_void, (num_pids * 4) as c_int)};
    pids.into_iter()
        .filter_map(|pid| -> Option<Process> {
            if pid == 0 {
                return None;
            }
            let mut buffer: Vec<u8> = vec![0; 1024];
            let sz = unsafe {processes_sys::proc_pidpath(pid, buffer.as_mut_ptr() as *mut c_void, 1024)};
            buffer.resize(sz as usize, 0);
            if let Ok(name) = String::from_utf8(buffer) {
                return Some(Process { pid, name });
            }
            None
        })
        .collect()
}