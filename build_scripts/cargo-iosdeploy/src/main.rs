use anyhow::Result;
use apple_bundle::prelude::*;
use cargo_metadata::{camino::Utf8PathBuf, Package};
use clap::Parser;
use serde::Deserialize;
use std::io::prelude::*;
use std::os::unix::fs::PermissionsExt;
use std::{
    fs::{self, File, Permissions},
    io::BufWriter,
    ops::Index,
    path::PathBuf,
    process::Command,
};
use structopt::StructOpt;

// Based on cargo-lipo; not sure if absolutely necessary, but :shrug:
fn create_universal_binary(
    project_settings: &ProjectSettings,
    package: &Package,
    targets: &Vec<&str>,
    is_release: bool,
    mold_dir: Option<String>,
    build_dir: &Utf8PathBuf,
) -> Result<Utf8PathBuf> {
    let mut libs: Vec<String> = Vec::new();
    let output_filename = package.name.clone();
    for target in targets.iter() {
        let mut cmd = Command::new("cargo");
        cmd.args([
            "build",
            "--manifest-path",
            package.manifest_path.as_str(),
            "--package",
            &package.name,
            "--target",
            target,
        ]);
        let mut rust_flags: Vec<String> = Vec::new();
        if let Some(ios_version) = &project_settings.ios_deployment_version {
            cmd.env("IPHONEOS_DEPLOYMENT_TARGET", ios_version);
            if let Some(sdks_rel) = &project_settings.sdks_root {
                let mut sdk_root = PathBuf::from(package.manifest_path.parent().unwrap());
                sdk_root.push(sdks_rel);
                sdk_root.push(format!("iPhoneOS{}.sdk", ios_version));
                if let Ok(sdk_root) = sdk_root.canonicalize() {
                    cmd.env("SDKROOT", &sdk_root);
                    rust_flags.push(format!("-Clink-arg=-L{}/usr/lib", sdk_root.as_os_str().to_str().unwrap()));
                    let private_root = sdk_root.join("System/Library/PrivateFrameworks");
                    cmd.env("PRIVATE_FW_ROOT", private_root);
                }
            }
        }
        if is_release {
            cmd.env("BUILD_RELEASE", "True");
            cmd.arg("--release");
        }
        if project_settings.is_tool {
            let info_plist = PathBuf::from(target).join("Info.plist.bin");
            rust_flags.push(format!("-Clink-arg=-Wl–sectcreate,__TEXT,__info_plist,{}",info_plist.to_str().unwrap()));
        }
        if let Some(ref mold_dir) = mold_dir {
            rust_flags.push(format!("-Clink-arg=-fuse-ld={mold_dir}"));
            rust_flags.push(format!("-Clink-arg=-Wl,--print-dependencies"));
        }
        if !rust_flags.is_empty() {
            cmd.env("CARGO_ENCODED_RUSTFLAGS", rust_flags.join("\x1f"));
        }
        let status = cmd.status()?;
        if !status.success() {
            anyhow::bail!("Failed to run ")
        }
        let mut artifact_path = build_dir.clone();
        artifact_path.push(target);
        if is_release {
            artifact_path.push("release");
        } else {
            artifact_path.push("debug");
        }
        artifact_path.push(&output_filename);
        libs.push(artifact_path.to_string());
    }

    let mut output = build_dir.join("universal");
    fs::create_dir_all(&output)?;
    output.push(&output_filename);
    let mut cmd = Command::new("lipo");
    cmd.args(["-create", "-output", output.as_str()]);
    for lib in libs.iter() {
        cmd.arg(&lib);
    }
    let status = cmd.status()?;
    if !status.success() {
        anyhow::bail!("Failed to run lipo command with error: {}", status);
    }
    if let Some(entitlements_src) = &project_settings.entitlements {
        let entitlements_path = package
            .manifest_path
            .parent()
            .unwrap()
            .join(entitlements_src);
        if !entitlements_path.exists() {
            anyhow::bail!("Entitlements file {} does not exist", entitlements_path);
        }
        let status = Command::new("ldid")
            .arg(output.as_str())
            .arg(format!("-S{}", entitlements_path.as_str()))
            .status()?;
        if !status.success() {
            anyhow::bail!("Failed to sign binary due to {:?}", status);
        }
    } else {
        let status = Command::new("ldid")
            .arg(output.as_str())
            .arg("-S")
            .status()?;
        if !status.success() {
            anyhow::bail!("Failed to sign binary due to {:?}", status);
        }
    }
    Ok(output)
}

fn create_info_plist(
    project_settings: &ProjectSettings,
    package: &Package,
    output_dir: &Utf8PathBuf,
) -> Result<()> {
    fs::create_dir_all(output_dir)?;
    let mut info_plist = apple_bundle::info_plist::InfoPlist {
        camera_and_microphone: CameraAndMicrophone {
            camera_usage_description: project_settings.camera_permission_string.clone(),
            microphone_usage_description: project_settings.microphone_permission_string.clone(),
        },
        naming: Naming {
            bundle_display_name: Some(project_settings.app_name.to_owned()),
            ..Default::default()
        },
        operating_system_version: OperatingSystemVersion {
            requires_iphone_os: Some(true),
            ..Default::default()
        },
        bundle_version: BundleVersion {
            bundle_info_dictionary_version: Some("6.0".to_owned()),
            bundle_version: Some("1.0".to_owned()),
            ..Default::default()
        },
        identification: Identification {
            bundle_identifier: project_settings.bundle_package.to_owned(),
            ..Default::default()
        },
        launch: Launch {
            bundle_executable: Some(package.name.clone()),
            ..Default::default()
        },
        launch_conditions: LaunchConditions {
            required_device_capabilities: Some(vec![DeviceCapabilities::Armv7]),
            ..Default::default()
        },
        categorization: Categorization {
            bundle_package_type: Some("APPL".to_owned()),
            ..Default::default()
        },
        background_execution: BackgroundExecution {
            ui_device_family: Some(vec![1]),
            ..Default::default()
        },
        launch_interface: LaunchInterface {
            launch_screen: Some(LaunchScreen {
                image_name: Some("LaunchImage".to_owned()),
                ..Default::default()
            }),
            ..Default::default()
        },

        orientation: Orientation {
            interface_orientation: Some(InterfaceOrientation::Portrait),
            supported_interface_orientations: Some(vec![
                InterfaceOrientation::Portrait,
                InterfaceOrientation::PortraitUpsideDown,
                InterfaceOrientation::LandscapeLeft,
                InterfaceOrientation::LandscapeRight,
            ]),
        },
        ..Default::default()
    };
    let mut info_plist_path = output_dir.clone();
    info_plist_path.push("Info.plist");
    info_plist.launch.bundle_executable = Some(package.name.clone());
    if project_settings.is_tool {
        info_plist_path.set_extension("plist.bin");
        apple_bundle::plist::to_file_binary(info_plist_path, &info_plist)?;
    } else {
        let file = std::fs::File::create(info_plist_path)?;
        apple_bundle::plist::to_writer_xml(file, &info_plist)?;
    }
    Ok(())
}

fn create_tool_bundle(install_root: &Utf8PathBuf, binary_path: &Utf8PathBuf) -> Result<()> {
    let binary_output_path = install_root
        .join("usr")
        .join("local")
        .join("bin")
        .join(binary_path.file_name().unwrap());
    fs::create_dir_all(binary_output_path.parent().unwrap())?;
    fs::copy(&binary_path, &binary_output_path)?;
    Ok(())
}

fn create_app_bundle(
    package: &Package,
    target_dir: &Utf8PathBuf,
    install_dir: &Utf8PathBuf,
    binary_path: &Utf8PathBuf,
) -> Result<()> {
    let app_bundle_root = install_dir
        .join("Applications")
        .join(format!("{}.app", package.name));
    fs::create_dir_all(&app_bundle_root)?;
    let info_plist_dst = app_bundle_root.join("Info.plist");
    let info_plist_src = target_dir.join("Info.plist");
    fs::copy(info_plist_src, info_plist_dst)?;
    let mut binary_output_path = app_bundle_root.clone();
    binary_output_path.push(&package.name);
    fs::copy(binary_path, &binary_output_path)?;
    Command::new("ldid")
        .arg(binary_output_path.as_str())
        .arg("-S")
        .status()?;
    Command::new("touch")
        .arg(app_bundle_root.join("LaunchImage.png"))
        .status()?;
    // Do I need this, tho?
    fs::set_permissions(binary_path, Permissions::from_mode(0o755))?;
    Ok(())
}

struct Control {
    package: String,
    name: String,
    version: String,
    architecture: String,
    description: String,
    author: String,
    section: String,
    installed_size: u64,
}

impl Control {
    fn serialise<W: std::io::Write>(&self, writer: W) -> Result<()> {
        let mut writer = BufWriter::new(writer);
        writer.write(format!("Package: {}\n", self.package).as_bytes())?;
        writer.write(format!("Name: {}\n", self.name).as_bytes())?;
        writer.write(format!("Description: {}\n", self.description).as_bytes())?;
        writer.write(format!("Architecture: {}\n", self.architecture).as_bytes())?;
        writer.write(format!("Author: {}\n", self.author).as_bytes())?;
        writer.write(format!("Section: {}\n", self.section).as_bytes())?;
        writer.write(format!("Version: {}\n", self.version).as_bytes())?;
        writer.write(format!("Installed-Size: {}\n", self.installed_size).as_bytes())?;
        Ok(())
    }
}

fn create_control_archive(
    deb_files: &Utf8PathBuf,
    package: &Package,
    install_root: &Utf8PathBuf,
    project_settings: &ProjectSettings,
) -> Result<Utf8PathBuf> {
    let control_path = deb_files.join("control");
    let control_archive_path = deb_files.join("control.tar.gz");
    let installed_size = fs_extra::dir::get_size(&install_root)? / 1024;
    let control = Control {
        package: project_settings.bundle_package.clone(),
        name: project_settings.app_name.clone(),
        version: package.version.to_string(),
        architecture: "iphoneos-arm".to_owned(),
        description: package.description.clone().unwrap_or_else(|| "".to_owned()),
        author: package.authors.join(","),
        section: project_settings.section.clone(),
        installed_size,
    };
    {
        let control_file = fs::File::create(&control_path)?;
        control.serialise(&control_file)?;
    }
    let status = Command::new("tar")
        .arg("czf")
        .arg(control_archive_path.as_str())
        .arg("-C")
        .arg(deb_files.as_str())
        .arg("control")
        .status()?;
    if !status.success() {
        anyhow::bail!("Failed to create control archive");
    }

    Ok(control_archive_path)
}

fn package_deb(
    build_dir: &Utf8PathBuf,
    package: &Package,
    install_root: &Utf8PathBuf,
    project_settings: &ProjectSettings,
) -> Result<Utf8PathBuf> {
    let deb_files = build_dir.join("deb_files");
    fs::create_dir_all(&deb_files)?;

    let output_deb = deb_files.join(format!("{}.deb", package.name));

    // debian-binary
    let debian_binary_path = deb_files.join("debian-binary");
    {
        let mut debian_binary = File::create(&debian_binary_path)?;
        debian_binary.write("2.0\n".as_bytes())?;
    }
    // control.tar.gz
    let control_archive_path =
        create_control_archive(&deb_files, &package, &install_root, project_settings)?;

    // data.tar.gz
    let data_archive_path = deb_files.join("data.tar.gz");
    {
        let status = Command::new("tar")
            .arg("czf")
            .arg(data_archive_path.as_str())
            .arg("-C")
            .arg(install_root.as_str())
            .arg(".")
            .status()?;
        if !status.success() {
            anyhow::bail!("Failed to create data.tar.gz due to {:?}", status);
        }
    }

    let status = Command::new("ar")
        .arg("-rc")
        .arg(output_deb.as_str())
        .arg(debian_binary_path.as_str())
        .arg(control_archive_path.as_str())
        .arg(data_archive_path.as_str())
        .status()?;

    if !status.success() {
        anyhow::bail!("Failed to create .deb");
    }

    Ok(output_deb)
}

#[derive(Deserialize)]
struct ProjectSettings {
    app_name: String,
    bundle_package: String,
    section: String,
    is_tool: bool,
    entitlements: Option<String>,
    sdks_root: Option<String>,
    ios_deployment_version: Option<String>,
    camera_permission_string: Option<String>,
    microphone_permission_string: Option<String>,
}
#[derive(Parser)]
struct Args {
    #[arg(long, default_value_t = false)]
    release: bool,
}

/// Automatically create universal libraries.
#[derive(StructOpt, Debug)]
#[structopt(name = "cargo-lipo", bin_name = "cargo")]
struct CargoInvocation {
    #[structopt(subcommand)]
    cmd: CargoCommand,
}

#[derive(StructOpt, Debug)]
enum CargoCommand {
    #[structopt(name = "iosdeploy")]
    Invocation(Invocation),
}

#[derive(StructOpt, Debug)]
struct Invocation {
    #[structopt(long)]
    release: bool,
    #[structopt(long)]
    mold_dir: Option<String>,
}

fn main() -> Result<()> {
    let cargo = CargoInvocation::from_args();
    match cargo.cmd {
        CargoCommand::Invocation(args) => main_impl(args),
    }
}

fn main_impl(args: Invocation) -> Result<()> {
    let cmd = cargo_metadata::MetadataCommand::new();
    let metadata = cmd.exec()?;
    let app_toml_txt = fs::read_to_string("app.toml")?;
    let project_settings: ProjectSettings = toml::from_str(&app_toml_txt)?;
    let targets = vec!["aarch64-apple-ios"];
    let is_release = args.release;
    let packages: Vec<_> = metadata
        .workspace_members
        .iter()
        .map(|index| metadata.index(index))
        .collect();
    if packages.len() != 1 {
        anyhow::bail!(
            "Can only have exactly one package being built. Have packages: {:#?}",
            packages
        );
    }
    let package = packages.first().unwrap();
    let install_root = metadata.target_directory.join("install_dir");
    create_info_plist(&project_settings, package, &metadata.target_directory)?;
    fs::create_dir_all(&install_root)?;
    let binary_path = create_universal_binary(
        &project_settings,
        &package,
        &targets,
        is_release,
        args.mold_dir,
        &metadata.target_directory,
    )?;
    if project_settings.is_tool {
        create_tool_bundle(&install_root, &binary_path)?;
    } else {
        create_app_bundle(package, &metadata.target_directory, &install_root, &binary_path)?;
    }
    package_deb(
        &metadata.target_directory,
        &package,
        &install_root,
        &project_settings,
    )?;

    Ok(())
}
