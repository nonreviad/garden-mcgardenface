use std::{collections::HashSet, path::PathBuf};

#[allow(non_camel_case_types)]
#[derive(Default)]
pub enum SDK {
    #[default]
    Default,
    iOS9_3,
    iOS10_3,
    iOS11_2,
    iOS12_1,
    iOS12_4,
    iOS13_0,
    iOS13_4,
    iOS13_5,
    iOS13_6,
    iOS13_7,
    iOS14_0,
    iOS14_1,
    iOS14_2,
    iOS14_3,
    iOS14_4,
    iOS14_5,
    iOS15_2,
    iOS15_5,
    iOS16_1,
    iOS16_2,
    iOS16_4,
}

impl SDK {
    fn subdir(&self) -> &'static str {
        match self {
            SDK::Default => panic!("No subdir for default"),
            SDK::iOS9_3 => "iPhoneOS9.3.sdk",
            SDK::iOS10_3 => "iPhoneOS10.3.sdk",
            SDK::iOS11_2 => "iPhoneOS11.2.sdk",
            SDK::iOS12_1 => "iPhoneOS12.1.sdk",
            SDK::iOS12_4 => "iPhoneOS12.4.sdk",
            SDK::iOS13_0 => "iPhoneOS13.0.sdk",
            SDK::iOS13_4 => "iPhoneOS13.4.sdk",
            SDK::iOS13_5 => "iPhoneOS13.5.sdk",
            SDK::iOS13_6 => "iPhoneOS13.6.sdk",
            SDK::iOS13_7 => "iPhoneOS13.7.sdk",
            SDK::iOS14_0 => "iPhoneOS14.0.sdk",
            SDK::iOS14_1 => "iPhoneOS14.1.sdk",
            SDK::iOS14_2 => "iPhoneOS14.2.sdk",
            SDK::iOS14_3 => "iPhoneOS14.3.sdk",
            SDK::iOS14_4 => "iPhoneOS14.4.sdk",
            SDK::iOS14_5 => "iPhoneOS14.5.sdk",
            SDK::iOS15_2 => "iPhoneOS15.2.sdk",
            SDK::iOS15_5 => "iPhoneOS15.5.sdk",
            SDK::iOS16_1 => "iPhoneOS16.1.sdk",
            SDK::iOS16_2 => "iPhoneOS16.2.sdk",
            SDK::iOS16_4 => "iPhoneOS16.4.sdk",
        }
    }
    fn sysroot(&self) -> Option<PathBuf> {
        match self {
            SDK::Default => None,
            _ => {
                let mut sysroot_path = PathBuf::from(file!());
                sysroot_path.pop();
                sysroot_path.pop();
                sysroot_path.pop();
                sysroot_path.push("sdks");
                sysroot_path.push(self.subdir());
                Some(sysroot_path)
            }
        }
    }
    fn private_framework_path(&self) -> String {
        if let Some(root) = self.sysroot() {
            let mut root = root;
            root.push("System");
            root.push("Library");
            root.push("PrivateFrameworks");
            root.to_str().unwrap().to_owned()
        } else {
            panic!("No sysroot :(");
        }
    }
}

#[derive(Default)]
pub struct ObjC {
    sources: Vec<String>,
    include_dirs: Vec<String>,
    frameworks: Vec<String>,
    private_frameworks: Vec<String>,
    has_objcc: bool,
    sdk_version: SDK,
    #[allow(dead_code)]
    debug: bool,
}

impl ObjC {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn sdk(&mut self, sdk_version: SDK) -> &mut Self {
        self.sdk_version = sdk_version;
        self
    }

    pub fn source(&mut self, file: &str) -> &mut Self {
        self.sources.push(file.to_owned());
        if file.ends_with(".mm") {
            self.has_objcc = true;
        }
        self
    }
    pub fn sources<F>(&mut self, files: F) -> &mut Self 
    where F: IntoIterator,
    F::Item : AsRef<str>{
        files.into_iter().for_each(|file| {
            self.source(file.as_ref());
        });
        self
    }
    pub fn include_dir(&mut self, dir: &str) -> &mut Self {
        self.include_dirs.push(dir.to_owned());
        self
    }
    pub fn framework(&mut self, framework: &str) -> &mut Self {
        self.frameworks.push(framework.to_owned());
        self
    }
    pub fn private_framework(&mut self, framework: &str) -> &mut Self {
        self.private_frameworks.push(framework.to_owned());
        self
    }
    pub fn compile(self, lib_name: &str) {
        let frameworks: HashSet<String> = self
            .frameworks
            .into_iter()
            .chain([
                "Foundation".to_owned(),
                "CoreFoundation".to_owned(),
            ])
            .collect();
        if !self.sources.is_empty() {
            let mut build = cc::Build::new();
            if !self.private_frameworks.is_empty() {
                build.flag(&format!("-F{}", self.sdk_version.private_framework_path()));
            }
            if self.has_objcc {
                build.cpp(true);
                build.flag("-std=c++1z");
                println!("cargo:rustc-link-arg=-ObjC++");
                println!("cargo:rustc-link-arg=-fobjc-exceptions");
                println!("cargo:rustc-link-arg=-fobjc-call-cxx-cdtors");
            }
            build.files(&self.sources);
            build.flag("-fobjc-arc");
            for include in self.include_dirs.iter() {
                build.flag(&format!("-I{include}"));
            }
            if let Some(sysroot) = self.sdk_version.sysroot() {
                let sysroot_flag = format!("--sysroot={}", sysroot.to_str().unwrap());
                build.flag(&sysroot_flag);
            }
            build.compile(lib_name);
            for source in self.sources.iter() {
                println!("cargo:rerun-if-changed={source}");
            }
        }
        if let Some(sysroot) = self.sdk_version.sysroot() {
            println!("cargo:rustc-link-search=framework={}", sysroot.to_str().unwrap());
        }
        if !self.private_frameworks.is_empty() {
            println!("cargo:rustc-link-search=framework={}", self.sdk_version.private_framework_path());
        }
        for framework in frameworks.iter().chain(self.private_frameworks.iter()) {
            println!("cargo:rustc-link-lib=framework={framework}");
        }
        println!("cargo:rustc-link-arg=-lobjc");
    }
}
